package main

import (
	"os"

	"github.com/rs/zerolog"
	"gitlab.com/cloudcreds.io/cloudcredentials/pkg/aws"
)

// Lets attempt to fake a Persister..

func main() {
	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	//log.Debug().Msg("Hello")
	logger.Debug().Msg("Constructor being called to create the Persister for AWS")
	awsCredentialPersister := aws.NewCloudCredentialPersistenceProvider(&logger)
	logger.Debug().Msg("Bogus data coming back from the Request from Vault to get the AWS STS Credentials")
	data := []byte("{\"data\":{\"access_key\": \"somekey\",\"secret_key\": \"somesecret\",\"security_token\": \"sometoken\"}}")
	logger.Debug().Msg("Persisting a record using 'file' format")
	//awsFormats := awsCredentialPersister.GetSupportedFormats()
	err := awsCredentialPersister.Persist(awsCredentialPersister.GetSupportedFormats().Ordinal("FILE"), data)
	if err != nil {
		logger.Error().Msgf("Here we go with Persist errors %s", err)
	}

	// We want, a map basically... []PersisterTypes
	// PersisterType (ID, Name)
	//awsCredentialPersister.Persist(awsFormats[], data)

}
