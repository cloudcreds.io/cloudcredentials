package aws

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	enumeration "gitlab.com/trukoda.com/enumerations"
)

// CredentialPersistenceProvider FILL ME OUT
type CredentialPersistenceProvider struct {
	log              *zerolog.Logger
	supportedFormats enumeration.Enumerator
	defaultFormat    string
}

// NewCloudCredentialPersistenceProvider constructor to create the AWS CredentialPersistenceProvider
func NewCloudCredentialPersistenceProvider(logger *zerolog.Logger) *CredentialPersistenceProvider {
	_formats := enumeration.NewEnumerator(enumeration.Element{ID: 0, Value: "FILE"})
	_formats.AddElement(enumeration.Element{ID: 1, Value: "ENVIRONMENT"})

	// Return sane defaults
	return &CredentialPersistenceProvider{
		log:              logger,
		supportedFormats: *_formats,
		defaultFormat:    _formats.Value(0),
	}
}

type Credential struct {
	Data struct {
		AccessKey     string `json:"access_key"`
		SecretKey     string `json:"secret_key"`
		SecurityToken string `json:"security_token"`
	}
}

func (aws *CredentialPersistenceProvider) Persist(format uint, data []byte) error {
	if aws.supportedFormats.Value(format) == "FILE" {
		var c Credential
		aws.log.Debug().Msgf("Data coming in was %s", data)
		json.Unmarshal(data, &c)
		aws.log.Debug().Msg("Unmarshalled the Data Structure")
		aws.log.Debug().Msgf("Data is %s", c)
		aws.log.Debug().Msgf("AccessKey is %s", c.Data.AccessKey)
		aws.log.Debug().Msgf("SecretKey is %s", c.Data.SecretKey)
	} else if aws.supportedFormats.Value(format) == "ENVIRONMENT" {
		aws.log.Debug().Msg("Formatting for environment Variables")
	} else {
		return errors.New("unsupported format requested")
	}
	fmt.Println(data)
	return nil
}

// GetSupportedFormats FILL ME OUT
func (aws *CredentialPersistenceProvider) GetSupportedFormats() *enumeration.Enumerator {
	return &aws.supportedFormats
}
