package provider

import (
	"github.com/hashicorp/go-plugin"
	"net/rpc"
)

// CloudCredentialRequestInput Is the request to generate the cloud credentials
type CloudCredentialRequestInput struct {
	// URI is the Universal Resource Identifier that the implementer will leverage to determine what is being requested.
	URI  string
	// Request is the Key/Interface pairing that allows for the implementor to get all the parameters necessary to fulfill the request.
	Request map[string]interface{}
	Persister CredentialPersistenceProvider
}

// CloudCredentialProvider represents the Provider of credentials to a specific cloud platform
type CloudCredentialProvider interface {
	// GetCredential function will persist the credentials using the persister in the request to get the type of credential requested.
	// It will return true if the credential worked, and false if not, if not, the error will expose details.
	GetCredential(request *CloudCredentialRequestInput) error
}

//CloudCredentialProviderRPC an RPC version of the cloud credential provider
type CloudCredentialProviderRPC struct{ client *rpc.Client }

//Authenticate is the interface method that AuthenticationProvider implementors must implement
func (cp *CloudCredentialProviderRPC) GetCredential(request *CloudCredentialRequestInput) error {
	var credStatus bool
	err := cp.client.Call("Plugin.GetCredential", request, &credStatus)
	if err != nil {
		panic(err)
	}
	return nil
}

//AuthenticationProviderRPCServer is the RPC server that AuthenticationProviderRPC talks to, conforming to
//// the requirements of net/rpc
type CloudCredentialProviderRPCServer struct {
	Impl CloudCredentialProvider
}

//Authenticate server version of Authenticate
func (cp CloudCredentialProviderRPCServer) GetCredential(request *CloudCredentialRequestInput) error {
	err := cp.Impl.GetCredential(request)
	return err
}

// Define the Plugin for the CloudCredentialProvider Generically
//AuthenticationProviderPlugin the plugin definition
type CloudCredentialProviderPlugin struct {
	Impl CloudCredentialProvider
}

//Server an RPC Server
func (cp *CloudCredentialProviderPlugin) Server(*plugin.MuxBroker) (interface{}, error) {
	return &CloudCredentialProviderRPCServer{Impl: cp.Impl}, nil
}

//Client an RPC client
func (CloudCredentialProviderPlugin) Client(b *plugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &AuthenticationProviderRPC{client: c}, nil
}