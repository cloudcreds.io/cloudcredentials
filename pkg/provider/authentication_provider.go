package provider

import (
	"github.com/hashicorp/go-plugin"
	"net/rpc"
)

// AuthenticationProviderInput is the input type for the Authentication Provider Authenticate Function
// The method string is for denoting to the implementing providers the type of authentication being requested
// The request map is to pass the 'schema' required to authenticate using the requested method.
type AuthenticationProviderInput struct {
	Method  string
	Request map[string]interface{}
}

// AuthenticationProvider is the interface that will allow for the 'user' to authenticate and
// retrieve a 'token' albiet saml, json, a string, etc..
type AuthenticationProvider interface {

	// Authenticate is the interface method that AuthenticationProvider implementors must
	// implement if they wish to allow 'users' to authenticate.
	// It takes a AuthenticationProviderInput object
	// It returns the number of bytes written and any error encountered.
	Authenticate(request AuthenticationProviderInput) []byte
}

//AuthenticationProviderRPC an RPC version of the authentication provider
type AuthenticationProviderRPC struct{ client *rpc.Client }

//Authenticate is the interface method that AuthenticationProvider implementors must implement
func (ap *AuthenticationProviderRPC) Authenticate(request AuthenticationProviderInput) []byte {
	var resp []byte
	err := ap.client.Call("Plugin.Authenticate", request, &resp)
	if err != nil {
		panic(err)
	}
	return resp
}

//AuthenticationProviderRPCServer is the RPC server that AuthenticationProviderRPC talks to, conforming to
//// the requirements of net/rpc
type AuthenticationProviderRPCServer struct {
	Impl AuthenticationProvider
}

//Authenticate server version of Authenticate
func (ap AuthenticationProviderRPCServer) Authenticate(request AuthenticationProviderInput, resp *[]byte) error {
	*resp = ap.Impl.Authenticate(request)
	return nil
}

//AuthenticationProviderPlugin the plugin definition
type AuthenticationProviderPlugin struct {
	Impl AuthenticationProvider
}

//Server an RPC Server
func (ap *AuthenticationProviderPlugin) Server(*plugin.MuxBroker) (interface{}, error) {
	return &AuthenticationProviderRPCServer{Impl: ap.Impl}, nil
}

//Client an RPC client
func (AuthenticationProviderPlugin) Client(b *plugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &AuthenticationProviderRPC{client: c}, nil
}
