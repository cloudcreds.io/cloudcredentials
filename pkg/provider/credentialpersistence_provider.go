package provider

import (
	enumeration "gitlab.com/trukoda.com/enumerations"
	"net/rpc"

	"github.com/hashicorp/go-plugin"
)

// PersistenceFormat is the type for holding supported formats that
// the PersistanceProvider knows how to implement
type PersistenceFormat struct {
	ID   uint
	Name string
}

//PersistInput the input to persist
type PersistInput struct {
	Format uint
	Data   []byte
}

// CredentialPersistenceProvider is the interface that all Credential Persisters should implement
type CredentialPersistenceProvider interface {
	// Persist will persist the data to the proper format for the Credential Provider format
	// the uint should align with a Enum of SupportedFormats.
	Persist(input PersistInput) string
	GetSupportedFormats(input string) enumeration.Enumerator

}

//CredentialPersistenceProviderRPC Here is an implementation that talks over RPC
type CredentialPersistenceProviderRPC struct{ client *rpc.Client }

//Persist persists the credentials
func (c *CredentialPersistenceProviderRPC) Persist(input PersistInput) string {
	var resp string
	err := c.client.Call("Plugin.Persist", input, &resp)
	if err != nil {
		panic(err)
	}
	return resp
}

//GetSupportedFormats gets the formats supported by this persister
func (c *CredentialPersistenceProviderRPC) GetSupportedFormats(input string) enumeration.Enumerator {
	var resp enumeration.Enumerator
	err := c.client.Call("Plugin.GetSupportedFormats", input, &resp)
	if err != nil {
		panic(err)
	}

	return resp
}

//CredentialPersistenceRPCServer is the RPC server that CredentialRPC talks to, conforming to
// the requirements of net/rpc
type CredentialPersistenceRPCServer struct {
	//The actual implementation
	Impl CredentialPersistenceProvider
}

//Persist server version of the persist function
func (s *CredentialPersistenceRPCServer) Persist(input PersistInput, resp *string) error {
	*resp = s.Impl.Persist(input)
	return nil
}

//GetSupportedFormats the server version of the get supported formats function
func (s *CredentialPersistenceRPCServer) GetSupportedFormats(input string, resp *enumeration.Enumerator) error {
	*resp = s.Impl.GetSupportedFormats(input)
	return nil
}

//CredentialPersistencePlugin The plugin definition
type CredentialPersistencePlugin struct {
	Impl CredentialPersistenceProvider
}

//Server the RPC Server
func (p *CredentialPersistencePlugin) Server(*plugin.MuxBroker) (interface{}, error) {
	return &CredentialPersistenceRPCServer{Impl: p.Impl}, nil
}

//Client the RPC Client
func (CredentialPersistencePlugin) Client(b *plugin.MuxBroker, c *rpc.Client) (interface{}, error) {
	return &CredentialPersistenceProviderRPC{client: c}, nil
}
