module gitlab.com/cloudcreds.io/cloudcredentials

go 1.14

require (
	github.com/hashicorp/go-plugin v1.3.0 // indirect
	github.com/rs/zerolog v1.19.0
	gitlab.com/trukoda.com/enumerations v0.0.0-20200524072000-0f2a51630435
)
